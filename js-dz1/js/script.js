console.log(document.querySelector('.number'));

function getNumberOne() {
  //poluchaem perviy nomer
  const firstInput = document.querySelector('.number').value;
  
  return parseInt(firstInput);
}

function getNumberTwo() {
  //poluchaem vtoroy nomer
   const secondInput = document.querySelector('.numbertwo').value;
  
  return parseInt(secondInput);
  
}

function getNumbersInput() {
  getNumberOne();
  getNumberTwo();
}

function displayResult(resultMultiplNumber) {
  //Vyvodim resultat na ekran
  document.querySelector('.result').innerHTML = 'Result is:' + resultMultiplNumber;
  
}

function multiplNumbers(numberOne, numberTwo) {
  const multiplResult = numberOne * numberTwo;
  
  return multiplResult;
}

function clickButtonEvent() {
  const numberOneInput = getNumberOne();
  const numberTwoInput = getNumberTwo();
  const numbersInput = getNumbersInput();
  const multiplResult = multiplNumbers(numberOneInput, numberTwoInput);
  
  displayResult(multiplResult);
  
}

document.querySelector('.submit').onclick = clickButtonEvent;